package org.amicofragile.scalafp

object MyMath {
  def log(b : Double) = (x: Double) => Math.log(x) / Math.log(b)
  def pow(b : Double) = (x: Double) => Math.pow(b, x)
  def sum(x: Int, y: Int) = x + y

  def factZeros(n : Int) : Int = {
    val powers = (1 to Math.floor(log(5)(n)).toInt)
    powers.map(x => pow(5)(x)).map(x => Math.floor(n / x).toInt).reduce(sum)
  }
}

object MyMathApp extends App {
  val r1 = MyMath.factZeros(127)
  val r2 = MyMath.factZeros(127000)
  print(r1)
  print(r2)
}
