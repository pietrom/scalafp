package org.amicofragile.scalafp.collections

import scala.annotation.tailrec

sealed trait MyList [+A] {
  def map[B](f: A => B) : MyList[B] = {
    this match {
      case MyNil => MyNil
      case MyCons(h, t) => MyCons(f(h), t.map(f))
    }
  }
  def sum(f: A => Double) : Double = reduce[A, Double](this, (acc, curr) => acc + f(curr), 0)
  def product(f: A => Double) : Double = reduce[A, Double](this, (acc, curr) => acc * f(curr), 1)

  def reduce[TValue, TResult](as: MyList[TValue], f: (TResult, TValue) => TResult, seed: TResult) : TResult = {
    @tailrec
    def loop(items: MyList[TValue], acc: TResult): TResult = {
      items match {
        case MyNil => acc
        case MyCons(h, t) => loop(t, f(acc, h))
      }
    }

    loop(as, seed)
  }
}

case object MyNil extends MyList[Nothing] {
}

case class MyCons[+A](head: A, tail: MyList[A]) extends MyList[A] {
}

object MyList {
  def apply[A](as : A *) : MyList[A] = {
    if (as.isEmpty) {
      MyNil
    } else {
      MyCons(as.head, apply(as.tail: _*))
    }
  }

  def setHead[A](l : MyList[A], newHead: A): MyList[A] = {
    l match {
      case MyNil => MyCons(newHead, MyNil)
      case MyCons(h, t) => MyCons(newHead, t)
    }
  }
}

object MyListHelper {
  implicit class MyIntList[T <: Int](val v : MyList[T]) extends AnyVal {
    def sum(): Double = v.sum(x => x)
  }

  implicit class MyDoubleList[T <: Double](val v : MyList[T]) extends AnyVal {
    def sum(): Double = v.sum(x => x)
  }
}
