package org.amicofragile.scalafp.collections

import org.junit._
import MyListHelper._

class MyListTest {
  def identity(x:Int):Double = x
  val delta : Double = 0.001

  @Test
  def canCreateThroughApply : Unit = {
    val sut = MyList(1, 2, 3, 4, 5)
  }

  @Test
  def sumEmptyList:Unit = {
    val sut:MyList[Int] = MyNil
    Assert.assertEquals(0, sut.sum(identity), delta)
  }

  @Test
  def sumNotEmptyListOfInts:Unit = {
    val sut:MyList[Int] = MyList(1, 2, 3, 4, 5)
    Assert.assertEquals(15, sut.sum(), delta)
  }

  @Test
  def sumNotEmptyListOfDoubles:Unit = {
    val sut:MyList[Double] = MyList(1.1, 2.1, 3.1, 4.1, 5.1)
    Assert.assertEquals(15.5, sut.sum(), delta)
  }

  @Test
  def sumDoubles:Unit = {
    val sut:MyList[Int] = MyList(1, 2, 3, 4, 5)
    Assert.assertEquals(30, sut.sum(x => x * 2), delta)
  }

  @Test
  def sumLengths:Unit = {
    val sut:MyList[String] = MyList("a", "bb", "ccc")
    Assert.assertEquals(6, sut.sum(x => x.length), delta)
  }

  @Test
  def multiplyEmptyListItems: Unit = {
    val empty = MyList()
    Assert.assertEquals(1, empty.product(x => x), delta)
  }

  @Test
  def multiplyNotEmptyList: Unit = {
    val sut = MyList(10, 20, 30, 40)
    Assert.assertEquals(240000, sut.product(x => x), delta)
  }

  @Test
  def multiplyTriples: Unit = {
    val sut = MyList(10, 20, 30, 40)
    Assert.assertEquals(240000 * 81, sut.product(x => x * 3), delta)
  }

  @Test
  def setHeadOnEmptyList : Unit = {
    Assert.assertEquals(MyList(11), MyList.setHead(MyNil, 11))
  }

  @Test
  def setHeadOnNotEmptyList : Unit = {
    Assert.assertEquals(MyList(11, 2, 3), MyList.setHead(MyList(1, 2, 3), 11))
  }

  @Test
  def mapEmptyList : Unit = {
    val l : MyList[Int] = MyNil
    Assert.assertEquals(MyNil, l.map(x => x * 3))
  }

  @Test
  def mapNotEmptyList : Unit = {
    Assert.assertEquals(MyList(3, 6, 9, 12), MyList(1, 2, 3, 4).map(x => x * 3))
  }
}
